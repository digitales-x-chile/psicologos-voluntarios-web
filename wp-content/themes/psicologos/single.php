<?php /* Template Name: Single Post */ ?>
<?php get_header(); ?>

<div class="wrap">
<div class="col_12">

<?php if ( have_posts() ) while ( have_posts() ) : the_post(); ?>

	<div id="nav-above" class="navigation">
		<div class="nav-previous"><?php previous_post_link( '%link', '<span class="meta-nav">' . _x( '&larr;', 'Previous post link', 'twentyten' ) . '</span> %title' ); ?></div>
		<div class="nav-next"><?php next_post_link( '%link', '%title <span class="meta-nav">' . _x( '&rarr;', 'Next post link', 'twentyten' ) . '</span>' ); ?></div>
	</div><!-- #nav-above -->

	<div id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
		<h1 class="entry-title"><?php the_title(); ?></h1>

		<div class="entry-meta">
			<?php twentyten_posted_on(); ?>
		</div><!-- .entry-meta -->

		<div class="entry-content">
			<?php the_content(); ?>
			<?php wp_link_pages( array( 'before' => '<div class="page-link">' . __( 'Pages:', 'twentyten' ), 'after' => '</div>' ) ); ?>
		</div><!-- .entry-content -->

<?php if ( get_the_author_meta( 'description' ) ) : // If a user has filled out their description, show a bio on their entries  ?>
<?php endif; ?>

		<div class="entry-utility">
			<?php twentyten_posted_in(); ?>
			<?php edit_post_link( __( 'Edit', 'twentyten' ), '<span class="edit-link">', '</span>' ); ?>
		</div><!-- .entry-utility -->
	</div><!-- #post-## -->

	

	<?php comments_template( '', true ); ?>

<?php endwhile; // end of the loop. ?>

			
			
</div>

<?php get_sidebar(); ?>
</div>
<?php get_footer(); ?>
