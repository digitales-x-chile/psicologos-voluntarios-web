<?php
/**
 * The Header for our theme.
 *
 *
 * @subpackage Psicologos Voluntarios
 * @since Psicologos V 1.0
 */
?><!DOCTYPE html>

<html <?php language_attributes(); ?>
xmlns:og="http://opengraphprotocol.org/schema/"
xmlns:fb="http://www.facebook.com/2008/fbml"
>
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>" />
<title><?php wp_title( '|', true, 'right' );?>Psicologos Voluntarios</title>
<link rel="profile" href="http://gmpg.org/xfn/11" />
<link rel="stylesheet" type="text/css" media="all" href="<?php bloginfo( 'stylesheet_url' ); ?>" />
<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>" />
	<link rel="shortcut icon" href="<?php bloginfo('template_directory'); ?>/favicon.ico" />
    <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.4.2/jquery.min.js"></script>
    <script type="text/javascript" src="<?php bloginfo('template_directory'); ?>/html5.js"></script>
    
    <script type="text/javascript">

		var slidesTimer = true;

        $(document).ready(function()
        {
            if ($(".tab_content")) {
                $(".tab_content").hide();
                $("ul.tabs li:first").addClass("active").show();
                $(".tab_content:first").show();

                $("ul.tabs li a.titulo").click(function(){displaySlide(this)});
            }

			slidesTimer = window.setInterval(function(){displayNext();}, 5000);
			
        });

		var displayNext = function(){

			var slide = $("ul.tabs li.active");
			if($(slide).children("a.titulo").attr('href')!='#tab4'){
				displaySlide(slide.next().children("a.titulo"));
			}else{
				displaySlide($("ul.tabs li a.titulo[href=#tab1]"));
			}

		}


		var displaySlide = function(element){

			$("ul.tabs li").removeClass("active");
			$(element).parent().addClass("active");
			$(".tab_content").hide();

			var activeTab = $(element).attr("href");
			$(activeTab).fadeIn();
			return false;
	   }


    </script>
<?php
	/* We add some JavaScript to pages with the comment form
	 * to support sites with threaded comments (when in use).
	 */
	if ( is_singular() && get_option( 'thread_comments' ) )
		wp_enqueue_script( 'comment-reply' );

	/* Always have wp_head() just before the closing </head>
	 * tag of your theme, or you will break many plugins, which
	 * generally use this hook to add elements to <head> such
	 * as styles, scripts, and meta tags.
	 */
	wp_head();
?>
</head>



<body <?php body_class(); ?>>

<div id="header">
	
	<div id="user-nav">
		<ul id="location-web">
			<li><a class="active" href="#">sitio web</a></li>
			<li><a href="http://red.psicologosvoluntarios.cl/">sector voluntarios</a></li>
			<li class="facebook">
				<a href="http://www.facebook.com/pages/Psicologos-Voluntarios-de-Chile/124568870906720" target="_blank">facebook</a>
			</li>
			<li class="twitter"><a href="http://twitter.com/PsicVoluntarios" target="_blank">twitter</a></li>
			<li class="flickr"><a href="http://www.flickr.com/photos/psicvoluntarios/" target="_blank">flickr</a></li>
			<li class="youtube"><a href="http://www.youtube.com/user/PsicVoluntarios" target="_blank">youtube</a></li>
			<li class="contacto"><a href="<?php echo home_url( '/' ); ?>contacto/">contacto</a></li>
		</ul>
	</div>

	<div class="wrap">
		<div id="logo">
			<h1><a href="<?php echo home_url( '/' ); ?>" title="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>" >
				<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>
			</a></h1>
		</div>
		
		<div id="header-sidebar">
			<div id="fono">
				<h2>
					<small>fono contacto</small><br>
					(562)249 8537
				</h2>
			</div>
			
			<div id="search"></div>
		</div>

		<div id="menus">
			<?php wp_nav_menu( array( 'theme_location' => 'main', 'sort_column' => 'menu_order' )); ?>
		</div>
		
	</div>
</div>
