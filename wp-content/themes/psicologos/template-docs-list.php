<?php /* Template Name: Documents List */ ?>
<?php get_header(); ?>

<div class="wrap">
<div class="col_12">

<?php if ( have_posts() ) while ( have_posts() ) : the_post(); ?>

	<div id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
		<?php if ( is_front_page() ) { ?>
			<h2 class="entry-title"><?php the_title(); ?></h2>
		<?php } else { ?>	
			<h1 class="entry-title"><?php the_title(); ?></h1>
		<?php } ?>				

		<div class="entry-content">
			<?php the_content(); ?>
			<?php wp_link_pages( array( 'before' => '<div class="page-link">' . __( 'Pages:', 'twentyten' ), 'after' => '</div>' ) ); ?>

			<?php $documentGroups = get_pages(array('child_of' => $post->ID)); ?>
			<?php if($documentGroups): ?>
			<ul class="docgroups">
				<?php foreach($documentGroups as $post): setup_postdata($post); ?>
				<li>
					<?php the_post_thumbnail('pv-thumbnail'); ?>
					<h3><a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"><?php echo the_title(); ?></a></h3>
					<p class="description"><?php the_content('Ver Listado de Documentos'); ?></p>
				</li>
				<?php endforeach; ?>
			</ul>
			<?php endif; ?>

			<?php edit_post_link( __( 'Edit', 'twentyten' ), '<span class="edit-link">', '</span>' ); ?>
		</div><!-- .entry-content -->
	</div><!-- #post-## -->

	<?php comments_template( '', true ); ?>

<?php endwhile; ?>

</div>

<?php get_sidebar(); ?>
</div>
<?php get_footer(); ?>
