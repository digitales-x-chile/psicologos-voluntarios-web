<?php

/** Psicologos Voluntarios **/
/** Thumbnail automático para cada Post */

add_theme_support( 'post-thumbnails' );
add_image_size( 'pv-thumbnail' , 50, 50, true );
add_image_size( 'logo-thumbnail' , 100, 100, true );
add_image_size( 'new-thumbnail' , 150, 150, true );
add_image_size( 'home-thumbnail' , 480, 200, true );



/** Modificación del Excerpt **/

function new_excerpt_more($more) {
	global $post;
	return '... <a href="'. get_permalink($post->ID) . '">' . 'leer más ›' . '</a>';
}
add_filter('excerpt_more', 'new_excerpt_more');

function new_excerpt_length( $length ) {
	return 30;
}
add_filter( 'excerpt_length', 'new_excerpt_length' );


/** Custom Types **/

register_post_type( 'experiencias',
  array(
    'labels' => array(
      'name' => __( 'Experiencias' ),
      'singular_name' => __( 'Expetiencia' )
    ),
    'public' => true,
    'supports' => array (
          'title',
          'editor',
          'thumbnail',
              ),
              'exclude_from_search' => false,
  )
);

register_post_type( 'alianzas',
  array(
    'labels' => array(
      'name' => __( 'Alianzas' ),
      'singular_name' => __( 'Alianza' )
    ),
    'public' => true,
    'supports' => array (
          'title',
          'editor',
          'thumbnail',
              ),
              'exclude_from_search' => false,
  )
);


register_post_type(
	'colaboradores',
	array(
		'labels' => array(
			'name' => __( 'Colaboradores' ),
			'singular_name' => __( 'Colaborador' )
    	),
		'public' => true,
		'supports' => array (
			'title',
			'editor',
			'thumbnail',
			'custom-fields',
		),
		'exclude_from_search' => false,
  	)
);



/* Menus de Navegación */

add_theme_support( 'nav-menus' );
register_nav_menu('main', 'Main Navigation Menu');




/* Sidebars del sitio */

register_sidebar( array(
	'name' => 'Home',
	'id' => 'home',
	'description' => 'Sidebar en Portada del Sitio',
	'before_widget' => '<div id="%1$s" class="%2$s">',
	'after_widget' => '</div>',
	'before_title' => '<h2>',
	'after_title' => '</h2>',
));


register_sidebar( array(
	'name' => 'Home More',
	'id' => 'home_more',
	'description' => 'Sidebar la parte inferior del Home',
	'before_widget' => '<div id="%1$s" class="widget %2$s">',
	'after_widget' => '</div>',
	'before_title' => '<h2>',
	'after_title' => '</h2>',
));


register_sidebar( array(
	'name' => 'Corporativo',
	'id' => 'corporative',
	'description' => 'Sidebar para la sección corporativa del sitio',
	'before_widget' => '<div id="%1$s" class="widget %2$s">',
	'after_widget' => '</div>',
	'before_title' => '<h2>',
	'after_title' => '</h2>',
));

register_sidebar( array(
	'name' => 'Ayuda',
	'id' => 'help',
	'description' => 'Sidebar para la sección de ayuda',
	'before_widget' => '<div id="%1$s" class="widget %2$s">',
	'after_widget' => '</div>',
	'before_title' => '<h2>',
	'after_title' => '</h2>',
));


register_sidebar( array(
	'name' => 'General',
	'id' => 'general',
	'description' => 'Sidebar Genérico del Sitio',
	'before_widget' => '<div id="%1$s" class="widget %2$s">',
	'after_widget' => '</div>',
	'before_title' => '<h2>',
	'after_title' => '</h2>',
));


register_sidebar( array(
	'name' => 'Footer',
	'id' => 'footer',
	'description' => 'Sidebardel pié de página',
	'before_widget' => '<div id="%1$s" class="widget %2$s">',
	'after_widget' => '</div>',
	'before_title' => '<h2>',
	'after_title' => '</h2>',
));






/** Twenty Ten **/

function twentyten_posted_on() {
	printf( __( '<span class="%1$s"></span> %2$s <span class="meta-sep">por</span> %3$s', 'twentyten' ),
		'meta-prep meta-prep-author',
		sprintf( '<a href="%1$s" title="%2$s" rel="bookmark"><span class="entry-date">%3$s</span></a>',
			get_permalink(),
			esc_attr( get_the_time() ),
			get_the_date()
		),
		sprintf( '<span class="author vcard"><a class="url fn n" href="%1$s" title="%2$s">%3$s</a></span>',
			get_author_posts_url( get_the_author_meta( 'ID' ) ),
			sprintf( esc_attr__( 'View all posts by %s', 'twentyten' ), get_the_author() ),
			get_the_author()
		)
	);
}


function twentyten_posted_in() {
	// Retrieves tag list of current post, separated by commas.
	$tag_list = get_the_tag_list( '', ', ' );
	if ( $tag_list ) {
		$posted_in = __( 'Escrito en %1$s and tagged %2$s. Bookmark the <a href="%3$s" title="Permalink to %4$s" rel="bookmark">permalink</a>.', 'twentyten' );
	} elseif ( is_object_in_taxonomy( get_post_type(), 'category' ) ) {
		$posted_in = __( 'escrito en %1$s. Bookmark <a href="%3$s" title="Permalink to %4$s" rel="bookmark">permalink</a>.', 'twentyten' );
	} else {
		$posted_in = __( 'Bookmark <a href="%3$s" title="Permalink to %4$s" rel="bookmark">permalink</a>.', 'twentyten' );
	}
	// Prints the string, replacing the placeholders.
	printf(
		$posted_in,
		get_the_category_list( ', ' ),
		$tag_list,
		get_permalink(),
		the_title_attribute( 'echo=0' )
	);
}


function twentyten_comment( $comment, $args, $depth ) {
	$GLOBALS['comment'] = $comment;
	switch ( $comment->comment_type ) :
		case '' :
	?>
	<li <?php comment_class(); ?> id="li-comment-<?php comment_ID(); ?>">
		<div id="comment-<?php comment_ID(); ?>">
		<div class="comment-author vcard">
			<?php echo get_avatar( $comment, 40 ); ?>
			<?php printf( __( '%s <span class="says">dice:</span>', 'twentyten' ), sprintf( '<cite class="fn">%s</cite>', get_comment_author_link() ) ); ?>
		</div><!-- .comment-author .vcard -->
		<?php if ( $comment->comment_approved == '0' ) : ?>
			<em><?php _e( 'Your comment is awaiting moderation.', 'twentyten' ); ?></em>
			<br />
		<?php endif; ?>

		<div class="comment-meta commentmetadata"><a href="<?php echo esc_url( get_comment_link( $comment->comment_ID ) ); ?>">
			<?php
				/* translators: 1: date, 2: time */
				printf( __( '%1$s at %2$s', 'twentyten' ), get_comment_date(),  get_comment_time() ); ?></a><?php edit_comment_link( __( '(Edit)', 'twentyten' ), ' ' );
			?>
		</div><!-- .comment-meta .commentmetadata -->

		<div class="comment-body"><?php comment_text(); ?></div>

		<div class="reply">
			<?php comment_reply_link( array_merge( $args, array( 'depth' => $depth, 'max_depth' => $args['max_depth'] ) ) ); ?>
		</div><!-- .reply -->
	</div><!-- #comment-##  -->

	<?php
			break;
		case 'pingback'  :
		case 'trackback' :
	?>
	<li class="post pingback">
		<p><?php _e( 'Pingback:', 'twentyten' ); ?> <?php comment_author_link(); ?><?php edit_comment_link( __('(Edit)', 'twentyten'), ' ' ); ?></p>
	<?php
			break;
	endswitch;
}


/** Funciones para Manejar Attachments **/

function get_attachments($mimetype = false) {
	global $post;

	$args = array(
		'post_parent' => $post->ID,
		'post_status' => 'inherit',
		'post_type' => 'attachment',
		'order' => 'ASC',
		'orderby' => 'menu_order ID'
	);

	if($mimetype){
		$args['post_mime_type'] = $mimetype;
	}

	$attachments = get_children( $args );

	if ($attachments) {
		return $attachments;
	}

	return false;
}
