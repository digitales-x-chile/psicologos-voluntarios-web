<?php /* Template Name: Documents Detail */ ?>
<?php get_header(); ?>

<div class="wrap">
<div class="col_12">

<?php if ( have_posts() ) while ( have_posts() ) : the_post(); ?>

	<div id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
		<?php if ( is_front_page() ) { ?>
			<h2 class="entry-title"><?php the_title(); ?></h2>
		<?php } else { ?>	
			<h1 class="entry-title"><?php the_title(); ?></h1>
		<?php } ?>				

		<div class="entry-content">
			<?php the_content(); ?>
			<?php wp_link_pages( array( 'before' => '<div class="page-link">' . __( 'Pages:', 'twentyten' ), 'after' => '</div>' ) ); ?>
			
			<?php $documents = get_attachments(); ?>
			<?php if($documents): ?>
			<ul class="docs">
				<?php foreach($documents as $document): ?>
				<li>
					<?php echo wp_get_attachment_image($document->ID, 'pv-thumbnail', true); ?>
					<h3><a href="<?php echo wp_get_attachment_url($document->ID); ?>" title="<?php echo $document->post_title; ?>" target="_blank"><?php echo $document->post_title; ?></a></h3>
					<p class="description"><?php echo $document->post_content; ?></p>
					<!--
					<pre><?php
					echo 'Link: '.wp_get_attachment_url($document->ID)."\n";
					echo 'Dump: ';
					print_r($document);
					?></pre>
					-->
				</li>
				<?php endforeach; ?>
			</ul>
			<?php endif; ?>
			<?php edit_post_link( __( 'Edit', 'twentyten' ), '<span class="edit-link">', '</span>' ); ?>
		</div><!-- .entry-content -->
	</div><!-- #post-## -->

	<?php comments_template( '', true ); ?>

<?php endwhile; ?>

</div>

<?php get_sidebar(); ?>
</div>
<?php get_footer(); ?>
