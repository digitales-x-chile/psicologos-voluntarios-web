<?php
/**
 * Template Name: Home Page
 * @subpackage Psicologos_voluntarios
 * @since Psicologos V 1.0
 */
get_header(); ?>


<div id="importante" class="app">

	<blockquote class="col_10" >
		Psicólogos a lo largo de todo nuestro país ofrecen sus capacidades para apoyar de forma solidaria a quienes más lo necesitan. <small><a href="<?php echo home_url( '/' ); ?>como-ayudar/">mas info +</a></small></blockquote>
	<div id="ingreso-app" class="col_6">
		<a href="http://red.psicologosvoluntarios.cl">Forma parte de esto<br><small>click aquí para ser voluntario</small></a>
	</div>	
		
</div>


<div id="home" class="wrap">

	<div class="container col_12">
	    
	    <div class="news">
		    <ul class="tabs">
				<?php query_posts(array('cat' => '-5','showposts' => 4)); ?>
					<?php $i = 0; ?>
					<?php while (have_posts()): ?>
						<?php the_post();?>
					<?php $i++; ?>
				<li>
					<a href="#tab<?php echo $i ?>" class="titulo"><?php the_title() ?></a>
				</li>
				<?php endwhile; ?>
		    </ul>
		    
		    <div class="tab_container">
	                <?php $i = 0; ?>
	                <?php while (have_posts()): ?>
						<?php the_post();?>
					<?php $i++; ?>
					
				<div id="tab<?php echo $i ?>" class="tab_content">
					
					<div class="post-img"><?php the_post_thumbnail('home-thumbnail'); ?></div>
					
					<?php the_excerpt(); ?>
		     	</div>
	            <?php endwhile; ?>
	        </div>
        </div>
        
        <section id="socialNetwork">
        	<?php dynamic_sidebar('home_more'); ?>
        </section>
	</div>
	
	<aside id="home" class="col_4">
		<?php dynamic_sidebar('home'); ?>
	</aside>

	
</div>



<?php get_footer(); ?>
