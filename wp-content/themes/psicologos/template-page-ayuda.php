<?php /* Template Name: Ayuda Voluntaria */ ?>
<?php get_header(); ?>

<div class="wrap">
<div class="col_12">

<?php if ( have_posts() ) while ( have_posts() ) : the_post(); ?>

	<div id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
		<?php if ( is_front_page() ) { ?>
			<h2 class="entry-title"><?php the_title(); ?></h2>
		<?php } else { ?>	
			<h1 class="entry-title"><?php the_title(); ?></h1>
		<?php } ?>				

		<div class="entry-content">
			<?php the_content(); ?>
			<?php wp_link_pages( array( 'before' => '<div class="page-link">' . __( 'Pages:', 'twentyten' ), 'after' => '</div>' ) ); ?>
			<?php edit_post_link( __( 'Edit', 'twentyten' ), '<span class="edit-link">', '</span>' ); ?>
		</div><!-- .entry-content -->
	</div><!-- #post-## -->

	<?php comments_template( '', true ); ?>

<?php endwhile; ?>

</div>
<aside class="col_4">
	<?php dynamic_sidebar('help'); ?>
</aside>
</div>
<?php get_footer(); ?>
