<?php
/**
 * The Sidebar containing the primary and secondary widget areas.
 *
 * @package WordPress
 * @subpackage Psicologos_voluntarios
 * @since Psicologos V 1.0
 */
?>

<aside class="col_4">
	<?php dynamic_sidebar('general'); ?>
</aside>