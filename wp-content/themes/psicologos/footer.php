<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the id=main div and all content
 * after.  Calls sidebar-footer.php for bottom widgets.
 *
 * @package WordPress
 * @subpackage Twenty_Ten
 * @since Twenty Ten 1.0
 */
?>



<div id="pre-footer">
	<div class="wrap">
		<div id="colaboradores">
			<h5>Colaboradores <a class="mas" href="<?php echo home_url( '/' ); ?>acerca-de/colaboradores/">ver más</a></h5>
			
			<ul>
			<?php
				$args = array(
					'post_type' => 'colaboradores',
					'numberposts' => 999999,
					'orderby' => 'rand',
					'posts_per_page' => 999999
				);
				query_posts($args);
				while (have_posts()) : the_post();
			?>
				<li>
					<a href="<?php echo get_post_meta($post->ID, 'url', true); ?>" title="<?php the_title() ?>"><?php the_post_thumbnail('pv-thumbnail'); ?></a>
				</li>
			<?php endwhile; ?>
			
			</ul>	
		</div>

			
			<?php
				$args = array(
					'post_type' => 'alianzas',
					'numberposts' => 1,
					'orderby' => 'rand',
					'posts_per_page' => 1
				);

				query_posts($args);

				while (have_posts()) : the_post();
			?>

			
			<div id="experiencias">
				<h5>Alianzas</h5>
				<?php the_post_thumbnail('thumbnail'); ?>
				<?php the_excerpt(); ?>
				<a href="<?php echo home_url( '/' ); ?>acerca-de/alianzas/">ver más</a>
			</div>

			<?php endwhile; ?>


	</div>
</div>
	
<div id="footer">
	<div class="wrap">
		<div class="creditos col_10">
			
			<p><a href="http://creativecommons.org/licenses/by-nc/2.0/cl/" rel="license"><img border="0" src="http://i.creativecommons.org/l/by-nc/2.0/cl/88x31.png" alt="Creative Commons License"></a><span><b>PsicologosVoluntarios.cl 2010</b> está bajo una <a href="http://creativecommons.org/licenses/by-nc/2.0/cl/" rel="license">licencia Creative Commons</a></span></p>
		</div>
	</div>
</div>



<?php
	/* Always have wp_footer() just before the closing </body>
	 * tag of your theme, or you will break many plugins, which
	 * generally use this hook to reference JavaScript files.
	 */

	wp_footer();
?>


<script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-18115768-1']);
  _gaq.push(['_trackPageview']);
 
  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script>

 

</body>
</html>
