<?php

/*
  Plugin Name: APost
  Plugin URI: http://halles.cl/
  Description: A Simple way to display a random, latest or most commented X post on the sidebar.
  Author: Matías Halles y @eseceve
  Author URI: http://halles.cl/
  Version: 1.5
 */

class APost {

    var $plugin_folder = '';
    var $default_options = array(
        'title' => 'This is a Post',
        'category' => 1,
        'order' => 1,
        'amount' => 1
    );

    function APost()
    {
        $this->plugin_folder = get_option('home') . '/' . PLUGINDIR . '/apost/';
    }

    function init()
    {
        if (!$options = get_option('widget_apost'))
            $options = array();

        $widget_ops = array('classname' => 'widget_apost', 'description' => 'Displays a post from a selected category.');
        $control_ops = array('width' => 250, 'height' => 100, 'id_base' => 'apost');
        $name = 'A Post';

        $registered = false;
        foreach (array_keys($options) as $o) {
            if (!isset($options[$o]['order']))
                continue;

            $id = "apost-$o";
            $registered = true;
            wp_register_sidebar_widget($id, $name, array(&$this, 'widget'), $widget_ops, array('number' => $o));
            wp_register_widget_control($id, $name, array(&$this, 'control'), $control_ops, array('number' => $o));
        }
        if (!$registered) {
            wp_register_sidebar_widget('apost-1', $name, array(&$this, 'widget'), $widget_ops, array('number' => -1));
            wp_register_widget_control('apost-1', $name, array(&$this, 'control'), $control_ops, array('number' => -1));
        }
    }

    function widget($args, $widget_args = 1)
    {
        extract($args);
        global $post, $wpdb;

        if (is_numeric($widget_args))
            $widget_args = array('number' => $widget_args);
        $widget_args = wp_parse_args($widget_args, array('number' => -1));
        extract($widget_args, EXTR_SKIP);
        $options_all = get_option('widget_apost');
        if (!isset($options_all[$number]))
            return;

        $options = $options_all[$number];

        $sorts[1] = " ORDER BY post_modified_gmt DESC";
        $sorts[2] = " ORDER BY comment_count DESC";
        $sorts[3] = " ORDER BY RAND()";

        $category = $options["category"];
        $amount = $options["amount"];
        $sort = $sorts[$options["order"]];

        $params = array('type' => 'post',
            'child_of' => $category,
            'orderby' => 'name',
            'order' => 'ASC',
            'hide_empty' => true,
            'include_last_update_time' => false,
            'hierarchical' => 1,
            'pad_counts' => false);

        $in_categories[] = $category;
        foreach (get_categories($params) as $acat)
            $in_categories[] = $acat->cat_ID;
        $in_categories = implode(', ', $in_categories);

        $request = "SELECT * FROM $wpdb->posts
				LEFT JOIN $wpdb->term_relationships ON
				($wpdb->posts.ID = $wpdb->term_relationships.object_id)
				LEFT JOIN $wpdb->term_taxonomy ON
				($wpdb->term_relationships.term_taxonomy_id = $wpdb->term_taxonomy.term_taxonomy_id)
				WHERE $wpdb->posts.post_status = 'publish'
				AND $wpdb->term_taxonomy.taxonomy = 'category'
				AND $wpdb->term_taxonomy.term_id IN($in_categories)";
        $request .= $sort;
        $request .= " LIMIT " . $amount;

        $posts = $wpdb->get_results($request);

        echo $before_widget;
        echo $before_title . $options['title'] . $after_title;
        include('apost-content.php');
        echo $after_widget;
    }

    function control($widget_args = 1)
    {
        global $wp_registered_widgets;
        static $updated = false;

        if (is_numeric($widget_args))
            $widget_args = array('number' => $widget_args);
        $widget_args = wp_parse_args($widget_args, array('number' => -1));
        extract($widget_args, EXTR_SKIP);
        $options_all = get_option('widget_apost');
        if (!is_array($options_all))
            $options_all = array();

        if (!$updated && !empty($_POST['sidebar'])) {
            $sidebar = (string) $_POST['sidebar'];

            $sidebars_widgets = wp_get_sidebars_widgets();
            if (isset($sidebars_widgets[$sidebar]))
                $this_sidebar = & $sidebars_widgets[$sidebar];
            else
                $this_sidebar = array();

            foreach ($this_sidebar as $_widget_id) {
                if ('widget_apost' == $wp_registered_widgets[$_widget_id]['callback'] && isset($wp_registered_widgets[$_widget_id]['params'][0]['number'])) {
                    $widget_number = $wp_registered_widgets[$_widget_id]['params'][0]['number'];
                    if (!in_array("apost-$widget_number", $_POST['widget-id']))
                        unset($options_all[$widget_number]);
                }
            }
            foreach ((array) $_POST['widget_apost'] as $widget_number => $posted) {
                if (!isset($posted['order']) && isset($options_all[$widget_number]))
                    continue;

                $options = array();


                $options['title'] = $posted['title'];
                $options['category'] = $posted['category'];
                $options['order'] = $posted['order'];
                $options['amount'] = $posted['amount'];

                $options_all[$widget_number] = $options;
            }
            update_option('widget_apost', $options_all);
            $updated = true;
        }

        if (-1 == $number) {
            $number = '%i%';
            $values = $this->default_options;
        } else {
            $values = $options_all[$number];
        }

        include('apost-form.php');
    }

}

$lgc_blogs = new APost();
add_action('widgets_init', array($lgc_blogs, 'init'));