<p>
    <label for="widget_apost-<?php echo $number; ?>-title">Title:</label><br/>
    <input id="widget_apost-<?php echo $number; ?>-title" type="text" name="widget_apost[<?php echo $number; ?>][title]" value="<?php echo $values['title']; ?>" class="widefat">
</p>

<p>
    <label for="widget_apost-<?php echo $number; ?>-category">Category</label><br/>
    <?php
    $defaults = array(
        'hierarchical' => 1,
        'show_count' => 1,
        'name' => "widget_apost[$number][category]",
        'selected' => $values['category'],
        'id' => 'widget_apost-' . $number . '-category',
    );
    wp_dropdown_categories($defaults); ?>
</p>

<p>
    <label for="widget_apost-<?php echo $number; ?>-version">Version:</label>
    <select class="widefat" style="width: 100;" name="widget_apost[<?php echo $number; ?>][order]" id="widget_apost-<?php echo $number; ?>-version">
        <option value="1"<?php echo $values['order'] == '1' ? ' selected="selected"' : ''; ?>>Last Published</option>
        <option value="2"<?php echo $values['order'] == '2' ? ' selected="selected"' : ''; ?>>Most Active</option>
        <option value="3"<?php echo $values['order'] == '3' ? ' selected="selected"' : ''; ?>>Random</option>
    </select>
</p>

<p>
    <label for="widget_apost-<?php echo $number; ?>-amount">Amount:</label>
    <select class="widefat" style="width: 100;" name="widget_apost[<?php echo $number; ?>][amount]" id="widget_apost-<?php echo $number; ?>-amount">
    <?php for($i; $i<11; $i++): ?>
        <option value="<?php echo $i ?>"<?php echo $values['amount'] == $i ? ' selected="selected"' : ''; ?>><?php echo $i ?></option>
    <?php endfor; ?>
    </select>
</p>