<ul>
	<?php foreach($posts as $post): ?>
	<?php setup_postdata($post); ?>
  	<li>
  	
	<div class="apost-img"><?php the_post_thumbnail('pv-thumbnail'); ?></div>
  
	<div class="apost-content">
		<h3><?php the_title() ?></h3>
		<?php the_excerpt(); ?>
		<div class="more">
			<a href="<?php the_permalink(); ?>">leer más</a>
		</div>
	</div>
	
  	</li>
	<?php endforeach; ?>
</ul>